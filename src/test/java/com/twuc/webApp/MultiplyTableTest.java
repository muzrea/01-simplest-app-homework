package com.twuc.webApp;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@SpringBootTest
@AutoConfigureMockMvc
public class MultiplyTableTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void plusTest() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/api/tables/multiply")).andReturn();
        assertEquals(mvcResult.getResponse().getStatus(), 200);
    }

    @Test
    public void plusTestContentType() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/api/tables/multiply")).andReturn();
        assertEquals(mvcResult.getResponse().getContentType(), "text/plain");
    }

    @Test
    public void plusTestContent() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/api/tables/multiply")).andReturn();
        assertEquals(mvcResult.getResponse().getContentAsString(),"1*1=1\n" +
                "2*1=2\t2*2=4\n" +
                "3*1=3\t3*2=6\t3*3=9\n" +
                "4*1=4\t4*2=8\t4*3=12\t4*4=16\n" +
                "5*1=5\t5*2=10\t5*3=15\t5*4=20\t5*5=25\n" +
                "6*1=6\t6*2=12\t6*3=18\t6*4=24\t6*5=30\t6*6=36\n" +
                "7*1=7\t7*2=14\t7*3=21\t7*4=28\t7*5=35\t7*6=42\t7*7=49\n" +
                "8*1=8\t8*2=16\t8*3=24\t8*4=32\t8*5=40\t8*6=48\t8*7=56\t8*8=64\n" +
                "9*1=9\t9*2=18\t9*3=27\t9*4=36\t9*5=45\t9*6=54\t9*7=63\t9*8=72\t9*9=81\n");
    }
}

