package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@SpringBootTest
@AutoConfigureMockMvc
public class PlusTableTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void plusTest() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/api/tables/plus")).andReturn();
        assertEquals(mvcResult.getResponse().getStatus(), 200);
    }

    @Test
    public void plusTestContentType() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/api/tables/plus")).andReturn();
        assertEquals(mvcResult.getResponse().getContentType(), "text/plain");
    }

    @Test
    public void should_return_plus_table() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/api/tables/plus")).andReturn();
        assertEquals(mvcResult.getResponse().getContentAsString(),"1+1=2\n" +
                "2+1=3\t2+2=4\n" +
                "3+1=4\t3+2=5\t3+3=6\n" +
                "4+1=5\t4+2=6\t4+3=7\t4+4=8\n" +
                "5+1=6\t5+2=7\t5+3=8\t5+4=9\t5+5=10\n" +
                "6+1=7\t6+2=8\t6+3=9\t6+4=10\t6+5=11\t6+6=12\n" +
                "7+1=8\t7+2=9\t7+3=10\t7+4=11\t7+5=12\t7+6=13\t7+7=14\n" +
                "8+1=9\t8+2=10\t8+3=11\t8+4=12\t8+5=13\t8+6=14\t8+7=15\t8+8=16\n" +
                "9+1=10\t9+2=11\t9+3=12\t9+4=13\t9+5=14\t9+6=15\t9+7=16\t9+8=17\t9+9=18\n");
    }
}
