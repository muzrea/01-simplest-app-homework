package com.twuc.webApp;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

@RestController
public class PlusTable {

    @GetMapping("/api/tables/plus")
    @ResponseBody
    public String plus(HttpServletResponse response) {
        response.setContentType("text/plain");
        response.setCharacterEncoding("UTF-8");
        StringBuilder str = new StringBuilder();
        for (int i = 1; i < 10; i++) {
            for (int j = 1; j <= i; j++) {
                if (j == i) {
                    str.append(i + "+" + j + "=" + (j + i) + "\n");
                } else {
                    str.append(i + "+" + j + "=" + (j + i) + "\t");
                }
            }
        }
        return str.toString();
    }
}
